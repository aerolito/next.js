export declare function getPostCssPlugins(dir: string, defaults?: boolean): Promise<import('postcss').AcceptedPlugin[]>;
